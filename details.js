
window.addEventListener("load", () => {
  const xhr = new XMLHttpRequest();
  let url = new URLSearchParams(window.location.search);
  urlparam = url.get("cname");
  xhr.open("GET",`https://restcountries.eu/rest/v2/alpha/${urlparam}`, true );
  xhr.onload = function () {
    let detail = document.getElementById("detail");
    let data = JSON.parse(this.responseText);
    let ImgUrl = data.flag.substring(0, 30);
    let neighbourCountriesArrayList = [];
    for (let i = 0; i < data.borders.length; i += 3) {
      if (i < data.borders.length) {
        neighbourCountriesArrayList.push(data.borders.slice(i, i + 3));
      }
    }

    let strNeighbourCountriesImges = "";
    // console.log(neighbourCountriesArrayList);
    if(neighbourCountriesArrayList.length > 0 ){
    neighbourCountriesArrayList.map((countriesSubArr) => {
    let colStr = "";
    
    countriesSubArr.map((countryCode) => {
          console.log(countryCode.toLowerCase())
        colStr += `<div class="col-4 px-1 align-self-center ">
                    <img src="https://restcountries.eu/data/${countryCode.toLowerCase()}.svg" 
                    class="img-fluid border" alt="flag">
                    </div>`;
        });
    strNeighbourCountriesImges += `<div class="row mx-auto py-2">
                                        ${colStr}
                                        </div>`;
    });

    }else{
        strNeighbourCountriesImges += `<div class="row mx-auto py-2">
        <div class="col-12  align-self-center ">
                    <p class="text-center h6 font-weight-light text-primary">⚠️ No Neighbours</p>
                    <img src="alone.png" 
                    class="img-fluid" alt="">
                    </div>
        </div>`;
    }

    let strLangueges = "";
    data.languages.map((language) => {
      strLangueges += language.name + " ";
    });
    let strCurrencies = "";
    data.currencies.map((currency) => {
      strCurrencies += currency.name + " ";
    });

    let str = "";
    str += `                <div class="row justify-content-start">
    <div class="col-md-6">
        <h1>${data.name}</h1>
    </div>
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-md-4 pic">
                <img src="${data.flag}" class="img-fluid w-100">
            </div>
                <div class="col-md-4">
                    <div class="row det">
                    <div>native name :  ${data.nativeName} </div>
                    <div>capital : ${data.capital} </div>
                    <div>population :${data.population} </div>
                    <div>region : ${data.region}</div>
                    <div>sub-region : ${data.subregion}  </div>
                    <div>Area : ${data.area} <sup>2</sup> </div>
                    <div>country code :+${data.numericCode} </div>
                    <div>languages : ${strLangueges}</div>
                    <div>currency :${strCurrencies} </div>
                    <div>timezones : ${data.timezones} </div>
                
                </div>
        </div>
    </div>
</div>
</div>
<div class="container nei">
<div class="row">
    <div class="col-md-6 pt-2">
        <h3>Neighbouring Countries</h3>
    </div>
</div>
<div class="container-fluid pt-5 ">
${strNeighbourCountriesImges}
</div>
>`;

    detail.innerHTML = str;
  };
  xhr.send();
});
